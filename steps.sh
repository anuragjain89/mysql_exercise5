# For generating a backup.
mysqldump --add-drop-table -uroot -p exercise > exercise_backup.sql
# For restoring the backup.
mysql -uroot -p exercise < exercise_backup.sql